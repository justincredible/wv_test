from django.shortcuts import render
from core.forms import *

# Create your views here.
def Upload( request ):
    form = FileUploadForm()
    if request.method == "POST":
        form = FileUploadForm( request.POST, request.FILES )
        if form.is_valid():
            print 'OK'
    return render( request, "fileUpload.html", { "form": form } )
